package bo.entereza.celulares.dto;

public class Cliente {
    Integer iddatos;
    private String nombre;
    private String apellido;
    private String telefono;
    private String email;

    private String username;
    private String password;

    public Cliente() {
    }

    public Integer getIddatos() {
        return iddatos;
    }

    public void setIddatos(Integer iddatos) {
        this.iddatos = iddatos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getTelefono() {
        return telefono;
    }
    public String getUsername() {
        return username;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
   

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "iddatos=" + iddatos +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", telefono=" + telefono +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
