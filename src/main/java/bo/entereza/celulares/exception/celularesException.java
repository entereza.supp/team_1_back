package bo.entereza.celulares.exception;
/*
Fecha de creacion:1/26/2022
Creado por: Sara Salazar

*/
public class celularesException extends RuntimeException {
    private final Integer code;
    public celularesException(Integer code, String message) {
        super(message);
        this.code = code;

    }

    public celularesException(Integer code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    
}
