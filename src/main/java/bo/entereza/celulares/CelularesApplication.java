package bo.entereza.celulares;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/*
Fecha de creacion:1/26/2022
Creado por: Sara Salazar

*/
@SpringBootApplication
public class CelularesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CelularesApplication.class, args);
	}

}
