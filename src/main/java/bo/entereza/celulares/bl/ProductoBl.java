package bo.entereza.celulares.bl;

import bo.entereza.celulares.dao.ProductoDao;
import bo.entereza.celulares.dto.Producto;
import bo.entereza.celulares.exception.celularesException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductoBl {
    private final ProductoDao productoDao;

    @Autowired
    public ProductoBl(ProductoDao productoDao){this.productoDao=productoDao;}

    public String createProduct(Producto producto){
        return productoDao.createProduct(producto);

    }

    public Producto findProduct(String nombreProducto){
        if (nombreProducto == null ) {
            throw new celularesException(403, "The name can't be null");
        }
        return productoDao.findProduct(nombreProducto);
    }

    public String updateProduct(String nombreProducto,Producto producto) {
        return productoDao.updateProduct(nombreProducto,producto);

    }

    public String deleteProduct(String nombreProducto){
        return productoDao.deleteproducto(nombreProducto);
    }

    public List<Producto> totalProduct(){
        return productoDao.totalProduct();
    }

    public String activeProduct(String nombreProducto){
        return productoDao.activeProduct(nombreProducto);
    }
}

