package bo.entereza.celulares.bl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import bo.entereza.celulares.exception.*;
import bo.entereza.celulares.dao.ClienteDao;
import bo.entereza.celulares.dto.Cliente;

/*
Fecha de creacion:1/26/2022
Creado por: Sara Salazar

*/
@Component
public class ClienteBl {
    private final ClienteDao clienteDao;

    @Autowired
    public ClienteBl(ClienteDao clienteDao) {
        this.clienteDao = clienteDao;
    }
    public String createCustomer(Cliente cliente){
        return clienteDao.createCustomer(cliente);
        
    }
    public String createCustomerLogin(Cliente cliente){
        
        return clienteDao.createCustomerLogin(cliente);
        
    }
    public List<Cliente> findClient(String user,String pass ) {
        if (user == null ||pass == null ) {
            throw new celularesException(403, "Bad request: The password parameter is mandatory and can't be null or empty");
        }
        return clienteDao.findClient(user, pass);
    }


    

    
}
