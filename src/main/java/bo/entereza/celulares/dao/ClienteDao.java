package bo.entereza.celulares.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import bo.entereza.celulares.dto.Cliente;



/*
Fecha de creacion:1/26/2022
Creado por: Sara Salazar

*/
@Component
public class ClienteDao {
    private DataSource dataSource;

    @Autowired
    public ClienteDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    //Creacion de cliente
    public String createCustomer(Cliente cliente) {
        String msj="";
        String query = "INSERT INTO datos_usuario VALUES ((select max(c.iddatos_usuario) from datos_usuario c)+ 1,(?),(?),(?),(?))";

        try (
            Connection conn = dataSource.getConnection(); 
        PreparedStatement pstmt = conn.prepareStatement(query);

        ) {

        
            pstmt.setString(1, cliente.getNombre());
            pstmt.setString(2, cliente.getApellido());
            pstmt.setString(3, cliente.getTelefono());
            pstmt.setString(4, cliente.getEmail());

            pstmt.executeUpdate();

            System.out.println(cliente.getApellido());

            msj="Agregado Sara 1";
        } catch (SQLException ex) {
            ex.printStackTrace();
            // TODO gestionar correctamente la excepción
        }
        return msj;

    }
  //Creaciacion de Login
    public String createCustomerLogin(Cliente cliente) {
        String msj="";
        String query = "INSERT INTO datos_login VALUES ((select max(l.iddatos_login) from datos_login l)+1,(?),(?),(select max(c.iddatos_usuario) from datos_usuario c))";

        try (
            Connection conn = dataSource.getConnection(); 
            PreparedStatement pstmt = conn.prepareStatement(query);

        ) {

            pstmt.setString(1, cliente.getUsername());
            pstmt.setString(2, cliente.getPassword());
            

            pstmt.executeUpdate();

            msj="Agregado Sara login";
        } catch (SQLException ex) {
            ex.printStackTrace();
            // TODO gestionar correctamente la excepción
        }
        return msj;

    }
  //Match de pass y user
    public List<Cliente> findClient(String user, String pass ){
        List<Cliente> result = new ArrayList<>();
        String query = "SELECT c.nombre,c.apellidos" + " FROM datos_usuario c, datos_login l"
                + " WHERE l.datos_usuario_iddatos_usuario=c.iddatos_usuario "
                + " AND c.iddatos_usuario=l.datos_usuario_iddatos_usuario "
                + " AND l.username=(?)"
                + " AND l.password=(?)";
                
                

        try (Connection conn = dataSource.getConnection(); PreparedStatement pstmt = conn.prepareStatement(query);) {
            System.out.println(query);
            pstmt.setString(1, user);
            pstmt.setString(2, pass);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Cliente cliente = new Cliente();
                cliente.setNombre(rs.getString("nombre"));
                cliente.setApellido(rs.getString("apellidos"));
                

                result.add(cliente);
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            // TODO gestionar correctamente la excepción
        }
        return result;
    }
    
}