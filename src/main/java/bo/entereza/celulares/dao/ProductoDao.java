package bo.entereza.celulares.dao;

import bo.entereza.celulares.dto.Cliente;
import bo.entereza.celulares.dto.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProductoDao {
    private DataSource dataSource;

    @Autowired
    public ProductoDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Producto> totalProduct(){
        List<Producto> result = new ArrayList<>();
        String query = "SELECT * FROM productos WHERE productos.estado=1";



        try (Connection conn = dataSource.getConnection(); PreparedStatement pstmt = conn.prepareStatement(query);) {
            System.out.println(query);
           

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Producto producto= new Producto();
                producto.setIdproducto(rs.getInt("idproductos"));
                producto.setNombre(rs.getString("nombre"));
                producto.setDescripcion(rs.getString("descripcion"));
                producto.setPrecio_unidad(Double.parseDouble(rs.getString("precio_unidad")));
                producto.setCantidad(rs.getInt("cantidad"));
                producto.setImagen(rs.getString("direccion_imagen"));
                producto.setNombre_m(rs.getString("marca"));
                producto.setRank(rs.getInt("rank"));
                producto.setEstado(rs.getInt("estado"));

                result.add(producto);
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            // TODO gestionar correctamente la excepción
        }
        return result;
    }

    public String createProduct(Producto product) {
        String msj="";
        String query = "INSERT INTO productos VALUES ((select max(p.idproductos) from productos p)+1, (?),(?),(?),(?),(?),(?),(?),(1))";

        try (
                Connection conn = dataSource.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(query);

        ) {
            pstmt.setString(1,product.getNombre());
            pstmt.setString(2,product.getDescripcion());
            pstmt.setDouble(3,product.getPrecio_unidad());
            pstmt.setInt(4,product.getCantidad());
            pstmt.setString(5,product.getImagen());
            pstmt.setString(6,product.getNombre_m());
            pstmt.setInt(7,product.getRank());
           

            pstmt.executeUpdate();

            msj="Agregando Producto";
        } catch (SQLException ex) {
            ex.printStackTrace();
            // TODO gestionar correctamente la excepción
        }
        return msj;

    }

    public Producto findProduct(String nombreProducto){
        Producto result = new Producto();
        String query = "SELECT p.idproductos, p.nombre, p.descripcion,p.precio_unidad,p.cantidad,p.direccion_imagen,p.marca,p.rank,p.estado " +
                "FROM productos p   " +
                "WHERE p.nombre=(?) ";



        try (Connection conn = dataSource.getConnection(); PreparedStatement pstmt = conn.prepareStatement(query);) {
            System.out.println(query);
            pstmt.setString(1, nombreProducto);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                result.setIdproducto(rs.getInt("idproductos"));
                result.setNombre(rs.getString("nombre"));
                result.setDescripcion(rs.getString("descripcion"));
                result.setPrecio_unidad(Double.parseDouble(rs.getString("precio_unidad")));
                result.setCantidad(rs.getInt("cantidad"));
                result.setImagen(rs.getString("direccion_imagen"));
                result.setNombre_m(rs.getString("marca"));
                result.setRank(rs.getInt("rank"));
                result.setEstado(rs.getInt("estado"));

            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            // TODO gestionar correctamente la excepción
        }
        return result;
    }

    public String updateProduct(String nombreProducto,Producto producto){

        String msj1="";
        String query = "update productos    " +
                "set productos.nombre=(?) ,productos.descripcion=(?),productos.precio_unidad=(?),productos.cantidad=(?),productos.direccion_imagen=(?),productos.marca=(?),productos.rank=(?)   " +
                "where productos.nombre=(?)";

        try (
                Connection conn = dataSource.getConnection();
                PreparedStatement pstmt =  conn.prepareStatement(query);
        ) {

            pstmt.setString(1,producto.getNombre());
            pstmt.setString(2,producto.getDescripcion());
            pstmt.setDouble(3,producto.getPrecio_unidad());
            pstmt.setInt(4,producto.getCantidad());
            pstmt.setString(5,producto.getImagen());
            pstmt.setString(6,producto.getNombre_m());
            pstmt.setInt(7,producto.getRank());
            pstmt.setString(8,nombreProducto);
            //Integer rs= pstmt.executeUpdate();
            pstmt.executeUpdate();
            msj1="Actualizando producto";
        } catch (SQLException ex) {
            ex.printStackTrace();
            // TODO gestionar correctamente la excepción
        }
        return msj1;
    }

    public String deleteproducto(String nombreProducto){
        String msj1="";
        String query = "update productos    " +
                "set productos.estado=0  " +
                "where productos.nombre=(?)";

        try (
                Connection conn = dataSource.getConnection();
                PreparedStatement pstmt =  conn.prepareStatement(query);
        ) {
            
            pstmt.setString(1,nombreProducto);
            Integer rs= pstmt.executeUpdate();
            pstmt.executeUpdate();
            msj1="Borrado";
        } catch (SQLException ex) {
            ex.printStackTrace();
            // TODO gestionar correctamente la excepción
        }
        return msj1;
    }
    public String activeProduct(String nombreProducto){
        String msj1="";
        String query = "update productos    " +
                "set productos.estado=1  " +
                "where productos.nombre=(?)";

        try (
                Connection conn = dataSource.getConnection();
                PreparedStatement pstmt =  conn.prepareStatement(query);
        ) {
            
            pstmt.setString(1,nombreProducto);
            Integer rs= pstmt.executeUpdate();
            pstmt.executeUpdate();
            msj1="Activado";
        } catch (SQLException ex) {
            ex.printStackTrace();
            // TODO gestionar correctamente la excepción
        }
        return msj1;
    }

}
