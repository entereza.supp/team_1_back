package bo.entereza.celulares.api;

import bo.entereza.celulares.bl.ProductoBl;
import bo.entereza.celulares.dto.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
public class ProductoApi {
    ProductoBl productoBl;

    @Autowired
    public ProductoApi(ProductoBl productoBl) {
        this.productoBl=productoBl;

    }

    @CrossOrigin(origins = "http://localhost:8081")
    @PostMapping(value = "/product", consumes = { MediaType.APPLICATION_JSON_VALUE })
    public String createProduct(@RequestBody Producto producto) {
        return productoBl.createProduct(producto);

    }

    @CrossOrigin(origins = "http://localhost:8081")
    @GetMapping(value = "/totalproduct", produces = { MediaType.APPLICATION_JSON_VALUE })
    public List<Producto> totalProduct() {
        return productoBl.totalProduct();

    }

    @CrossOrigin(origins = "http://localhost:8081")
    @GetMapping(value = "/product/{name}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public Producto findProduct(@PathVariable (name="name") String name) {
        return productoBl.findProduct(name);

    }

    @CrossOrigin(origins = "http://localhost:8081")
    @PutMapping(value = "/productp/{nombreProducto}")
    public String updateProduct(@RequestBody Producto producto, @PathVariable(name = "nombreProducto") String nombreProducto) {
        return productoBl.updateProduct(nombreProducto,producto);
    }


    @CrossOrigin(origins = "http://localhost:8081")
    @PutMapping(value = "/productd/{nombreProducto}")
    public String deleteProduct(@PathVariable(name = "nombreProducto") String nombreProducto) {
        return productoBl.deleteProduct(nombreProducto);
    }


    @CrossOrigin(origins = "http://localhost:8081")
    @PutMapping(value = "/producta/{nombreProducto}")
    public String activeProduct(@PathVariable(name = "nombreProducto") String nombreProducto) {
        return productoBl.activeProduct(nombreProducto);
    }



    /* @CrossOrigin(origins = "http://localhost:8081")
    @DeleteMapping(value = "/productd/{nombreProducto}")
    public String deleteProduct( @PathVariable(name = "nombreProducto") String nombreProducto) {
        return productoBl.deleteProduct(nombreProducto);
    } */

  


}
