package bo.entereza.celulares.api;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import bo.entereza.celulares.bl.ClienteBl;
import bo.entereza.celulares.dto.Cliente;

import org.springframework.http.MediaType;

/*
Fecha de creacion:1/26/2022
Creado por: Sara Salazar

*/
/*
Fecha de creacion:1/26/2022
Creado por: Sara Salazar

*/
@RestController()
public class celulares {
    ClienteBl clienteBl;



    @Autowired
    public celulares(ClienteBl clienteBl) {
        this.clienteBl = clienteBl;
      
    }
    //Crear usuario***
    @CrossOrigin(origins = "http://localhost:8081")
    @PostMapping(value = "/customer", consumes = { MediaType.APPLICATION_JSON_VALUE })
    public String createCustomer(@RequestBody Cliente cliente) {
        return clienteBl.createCustomer(cliente);

    }

    // Create login
    
    @CrossOrigin(origins = "http://localhost:8081")
    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String createCustomerLogin(@RequestBody Cliente cliente) {
        return clienteBl.createCustomerLogin(cliente);

    } 

    //Ingresar Login
    @CrossOrigin(origins = "http://localhost:8081")
    @GetMapping(value = "/cliente/{user}/{pass}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Cliente> findClientes(@PathVariable(name = "user") String user, @PathVariable(name = "pass") String pass) {
        return clienteBl.findClient(user,pass);
    }
    



    
}
