-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema ventas_celulares
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema ventas_celulares
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ventas_celulares` DEFAULT CHARACTER SET utf8mb4 ;
USE `ventas_celulares` ;

-- -----------------------------------------------------
-- Table `ventas_celulares`.`datos_usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ventas_celulares`.`datos_usuario` (
  `iddatos_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL DEFAULT NULL,
  `apellidos` VARCHAR(45) NULL DEFAULT NULL,
  `telefono` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`iddatos_usuario`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `ventas_celulares`.`datos_login`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ventas_celulares`.`datos_login` (
  `iddatos_login` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NULL DEFAULT NULL,
  `password` VARCHAR(45) NULL DEFAULT NULL,
  `datos_usuario_iddatos_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`iddatos_login`),
  INDEX `fk_datos_login_datos_usuario_idx` (`datos_usuario_iddatos_usuario` ASC),
  CONSTRAINT `fk_datos_login_datos_usuario`
    FOREIGN KEY (`datos_usuario_iddatos_usuario`)
    REFERENCES `ventas_celulares`.`datos_usuario` (`iddatos_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `ventas_celulares`.`productos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ventas_celulares`.`productos` (
  `idproductos` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL DEFAULT NULL,
  `descripcion` VARCHAR(45) NULL DEFAULT NULL,
  `precio_unidad` DOUBLE NULL DEFAULT NULL,
  `cantidad` INT(11) NULL DEFAULT NULL,
  `direccion_imagen` VARCHAR(45) NULL DEFAULT NULL,
  `marca` VARCHAR(45) NULL DEFAULT NULL,
  `rank` INT(11) NULL DEFAULT NULL,
  `estado` INT(11) NOT NULL,
  PRIMARY KEY (`idproductos`),
  UNIQUE INDEX `nombre_UNIQUE` (`nombre` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 15
DEFAULT CHARACTER SET = utf8mb4;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
